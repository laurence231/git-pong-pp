def fizzbuzzing(number: int):
    output = ""
    if number%3 != 0 and number%5 != 0 and number%7 !=0:
        output += str(number)
    if number%3 == 0:
        output += "Fizz"
    if number%5 == 0:
        output += "Buzz"
    if number%7 == 0:
        output += "Pop"
    return output


def testFB(number):
    for i in range(1, number+1, 1):
        print(i, fizzbuzzing(i), sep =" : ")

number = input("Input the number up to which you wish to see Fizz-Buzz-Poping! :")
testFB(int(number))
